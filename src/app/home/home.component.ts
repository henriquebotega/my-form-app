import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent {
  vehicleNewOrNot: Boolean = false;

  brandOfVehicle: Object[] = [
    { value: "brand-audi", title: "Audi" },
    { value: "brand-tesla", title: "Tesla" },
    { value: "brand-bmw", title: "BMW" }
  ];

  doorsOfVehicle: Object[] = [
    { value: "brand-three", title: "3" },
    { value: "brand-five", title: "5" },
    { value: "brand-nine", title: "9" }
  ];

  colorsOfVehicle: Object[] = [
    { value: "brand-white", title: "White" },
    { value: "brand-black", title: "Black" },
    { value: "brand-red", title: "Red" },
    { value: "brand-blue", title: "Blue" },
    { value: "brand-green", title: "Green" },
    { value: "brand-yellow", title: "Yellow" }
  ];

  typesOfVehicle: Object[] = [
    { value: "type-suv", title: "SUV" },
    { value: "type-sedan", title: "Sedan" },
    { value: "type-truck", title: "Truck" }
  ];

  myForm = new FormGroup({
    vehicle_brand: new FormControl("", [Validators.required]),
    vehicle_name: new FormControl("", [Validators.required]),
    vehicle_color: new FormControl("", [Validators.required]),
    vehicle_new: new FormControl("", [Validators.required]),
    vehicle_doors: new FormControl("", [Validators.required]),
    vehicle_type: new FormControl("", [Validators.required]),
    vehicle_year_manufacture: new FormControl("", [Validators.required])
  });

  onFormSubmit() {
    if (this.myForm.valid) {
      this.myForm.reset();
      alert("Saved successfully");
    }
  }
}
