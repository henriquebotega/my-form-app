import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MyMaterialModule } from "../my-material/my-material.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, MyMaterialModule]
})
export class HomeModule {}
